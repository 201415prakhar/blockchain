import java.io.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import org.json.*;
import java.net.*;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {

        for(int page = 0 ; page<=25 ; page+=25){
            String url = "https://blockstream.info/api//block/000000000000000000076c036ff5119e5a5a74df77abf64203473364509f7732/txs/" + page;

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .build();

            try {
                client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .thenApply(App::parse)
                        .join();


            } catch(Exception e) {
                System.out.println("ERROR!");
                System.out.println(e);
            }
        }



        //System.out.println(edge.size());
        getTop10Blocks(edge);
    }

    static List<List<String>> edge = new Vector<>();
    public static String parse(String responseBody) {
        JSONArray nations = new JSONArray(responseBody);
        //System.out.println(nations.length());

        int cnt =-1;
        try {
            for (int i = 0; i < nations.length(); i++) {
//                if(i!=5) continue;
                JSONObject block = nations.getJSONObject(i);
                String txid = block.getString("txid");
                JSONArray ip = block.getJSONArray("vin");
                int ip_len = block.getJSONArray("vin").length();
                //System.out.println("parent "+ txid);
                if(!map.containsKey(txid)) {
                    map.put(txid, ++cnt);
                    rmap.put(map.get(txid),txid);
                }

                if(!master.contains(map.get(txid))) {
                    master.add(map.get(txid));
                }

                for(int j=0;j<ip_len;j++){
                    JSONObject child =  ip.getJSONObject(j);
                    String tx_child = child.getString("txid");
                    //System.out.println("child "+ tx_child);
                    List<String> temp = new Vector<>();
                    temp.add(tx_child);temp.add(txid);
                    edge.add(temp);

                    if(!map.containsKey(tx_child)) {
                        map.put(tx_child, ++cnt);
                        rmap.put(map.get(tx_child),tx_child);
                    }

                }

            }


        } catch(Exception e){
            System.out.println(e);
        }
        return "null";
    }


    static Set<Integer>  master = new HashSet<>();
    static Map<Integer,String> rmap = new HashMap<>();
    static Map<String,Integer> map = new HashMap<>();

    static void getTop10Blocks( List<List<String>> list){
        System.out.println("Top 10  Largest Ancestory Sets are");

        int[][] edges = new int[list.size()][2];
        for(int i=0;i<edges.length;i++){
            String u = list.get(i).get(0);
            int ui = map.get(u);
            edges[i][0] = ui;
            String v = list.get(i).get(1);
            int vi = map.get(v);
            edges[i][1] = vi;
        }

       // System.out.println("-----------");
//        for(int i=0;i<edges.length;i++)
//            System.out.println(Arrays.toString(edges[i]));
//
//        System.out.println(map.size());

        System.out.println("------------------");

        int N= map.size();
        List<List<Integer>> ans = getAncestors(N,edges);
        //System.out.println(ans);

//        List<List<Integer>> fans = new Vector<>();
//        for(List<Integer> l : ans){
//           List<Integer> temp =   l.stream().filter(x->master.contains(x)).collect(Collectors.toList());
//           fans.add(temp);
//        }
//
//        System.out.println(fans);


        List<int[]> sortlist = new Vector<>();
        for(int i=0;i<ans.size();i++){
            int sz = ans.get(i).size();
            sortlist.add(new int[] {i,sz});
        }

        Collections.sort(sortlist,(o1,o2)->(o2[1]-o1[1]));
        for(int i=0;i<sortlist.size();i++){
            if(i>9) break;
            int block_id_int =  sortlist.get(i)[0];
            int sz = sortlist.get(i)[1];
            String block_id = rmap.get(block_id_int);
            System.out.println(block_id+" has Ancestory set size  = "+sz + " ");

        }


    }

    public static List<List<Integer>> getAncestors(int n, int[][] edges) {
        Map<Integer, List<Integer>> parentToKids = new HashMap<>();
        for (int[] e : edges) {
            parentToKids.computeIfAbsent(e[0], l -> new ArrayList<>()).add(e[1]);
        }
        List<List<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < n; ++i) {
            ans.add(new ArrayList<>());
        }
        for (int i = 0; i < n; ++i) {
            dfs(i, i, ans, parentToKids);
        }
        return ans;
    }
    private static void dfs(int ancestor, int kid, List<List<Integer>> ans, Map<Integer, List<Integer>> parentToKids) {
        List<Integer> ancestors = ans.get(kid);
        if (ancestors.isEmpty() || ancestors.get(ancestors.size() - 1) != ancestor) {
            if (ancestor != kid) {
                ancestors.add(ancestor);
            }
            for (int grandKid : parentToKids.getOrDefault(kid, Arrays.asList())) {
                dfs(ancestor, grandKid, ans, parentToKids);
            }
        }
    }



}
